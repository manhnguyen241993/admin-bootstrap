($ => {
    const js_checkboxes = () => {
        $("[data-checkboxes]").each(function() {
            const me = $(this),
                group = me.data('checkboxes'),
                role = me.data('checkbox-role');

            me.change(function() {
                let all = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"])'),
                    checked = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"]):checked'),
                    dad = $('[data-checkboxes="' + group + '"][data-checkbox-role="dad"]'),
                    total = all.length,
                    checked_length = checked.length;

                if (role === 'dad') {
                    me.is(':checked') ? all.prop('checked', true) : all.prop('checked', false);
                }else{
                    checked_length >= total ? dad.prop('checked', true) : dad.prop('checked', false);
                }
            });
        });
    };
    $(() => {
        js_checkboxes();
    });
})(jQuery);
