($ => {
    const js_scroll_to_top = () => {
        let $selector = $(".page-to-top");
        if ($selector.length < 1) { return; }
        let doc_height = $(document).height();
        $(window).on("scroll", () => {
            let offset_top = $(window).scrollTop();
            (offset_top > doc_height / 2) ? $selector.addClass("fixed") : $selector.removeClass("fixed");

        });
        $selector.on("click", (e) => {
            e.preventDefault();
            $("html,body").animate({
                scrollTop: 0
            }, 800);
        });

    };
    $(() => {
        js_scroll_to_top();
        // PRELOADER
        if (sessionStorage.getItem("isFirstRun")) { $("#preloader").addClass("d-none"); }
        $(window).on("load", function (e) { $("#preloader").fadeOut(); this.sessionStorage.setItem("isFirstRun", true); });
        setTimeout(function () { $("#preloader").fadeOut(); this.sessionStorage.setItem("isFirstRun", true); }, 4000);

    });
})(jQuery);